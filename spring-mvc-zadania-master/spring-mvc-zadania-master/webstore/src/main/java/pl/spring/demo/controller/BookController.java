package pl.spring.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import pl.spring.demo.constants.ModelConstants;
import pl.spring.demo.constants.ViewNames;
import pl.spring.demo.service.BookService;
import pl.spring.demo.to.BookTo;

/**
 * Book controller
 * 
 * @author mmotowid
 *
 */
@Controller
@RequestMapping("/books")
public class BookController {
	
	@Autowired
	private BookService bookService;

	@RequestMapping
	public String list(Model model) {
		// TODO: implement default method
		return ViewNames.BOOKS;
	}

	/**
	 * Method collects info about all books
	 */
	
	@RequestMapping("/all")
	public ModelAndView allBooks() {
		ModelAndView modelAndView = new ModelAndView();
		List<BookTo> listOfAllBooks = bookService.findAllBooks();
		modelAndView.setViewName(ViewNames.BOOKS);
		modelAndView.addObject(ModelConstants.BOOK_LIST, listOfAllBooks);
		return modelAndView;
	}

	// TODO: here implement methods which displays book info based on query
	// arguments
	@RequestMapping("/book")
	public ModelAndView showDetails(Long id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ViewNames.BOOK);
		for (BookTo book : bookService.findAllBooks()) {
			if (book.getId() == id) {
				modelAndView.addObject(ModelConstants.BOOK,book);
			}
		}
		return modelAndView;
	}
	
	@RequestMapping("/found")
	public ModelAndView searchBooksByTitle(@RequestParam("titlePrefix") String title, @RequestParam("authorPrefix") String author) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ViewNames.BOOKS);
		List<BookTo> books = new ArrayList<BookTo>();
		books.addAll(bookService.findBooksByTitle(title));
		books.addAll(bookService.findBooksByAuthor(author));
		modelAndView.addObject(ModelConstants.BOOK_LIST, books);
		return modelAndView;
	}
	

	// TODO: Implement GET / POST methods for "add book" functionality

	/**
	 * Binder initialization
	 */
	@InitBinder
	public void initialiseBinder(WebDataBinder binder) {
		binder.setAllowedFields("id", "title", "authors", "status");
	}

}
